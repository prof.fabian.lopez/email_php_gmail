<?php

//librerias
  require 'PHPMailer/PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
$mail->IsSMTP();

// si no les llega el mail, ingrese a este link y permitanle al google Acceso de aplicaciones poco seguras, activarlo
// https://myaccount.google.com/u/0/lesssecureapps?pli=1

//Configuracion servidor mail
$mail->From = "quien lo envia"; //remitente
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'tls'; //seguridad
$mail->Host = "smtp.gmail.com"; // servidor smtp
$mail->Port = 587; //puerto
$mail->Username ='usuariodegmail@gmail.com'; //nombre usuario
$mail->Password = 'password'; //contraseña
//Agregar Remitente
//$mail->SetFrom("no-reply@misitio", 'Mi sitio WEB');
//Agregar destinatario
$mail->AddAddress($_POST['email']);
$mail->Subject = $_POST['subject'];
$mail->Body = $_POST['message'];

//Avisar si fue enviado o no y dirigir al index
if ($mail->Send()) {
    echo'<script type="text/javascript">
           alert("Enviado Correctamente");
        </script>';
} else {
    echo'<script type="text/javascript">
           alert("NO ENVIADO, intentar de nuevo");
        </script>';
}
